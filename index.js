console.log("Hello World");

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

	function helloVisitor(){
	let fullName = prompt('What is your Name?')
	let age = prompt('How old are you?')
	let location = prompt('Where do you live?')
	
	console.log("Hello " + fullName);
	console.log("You are " + age + " years old.");
	console.log("You live in " + location);
}
helloVisitor();

function thankYouMessage(){
	alert('Thank you for your input!');
}
thankYouMessage();



/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

// second function here:


function favoriteMusicians(){
	let functionFavorite1 = "Blackpink";
	let functionFavorite2 = "Whitney Houston";
	let functionFavorite3 = "Alicia Keys";
	let functionFavorite4 = "Westlife";
	let functionFavorite5 = "Lani Misalucha";
	
	console.log (1 + ". " + functionFavorite1);
	console.log(2 + ". " + functionFavorite2);
	console.log(3 + ". " + functionFavorite3);
	console.log(4 + ". " + functionFavorite4);
	console.log(5 + ". " + functionFavorite5);
}
favoriteMusicians();


/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
let globalVar = "Favorite Movies"

function favoriteMovies(){
	const functionMovie1 = "Mulan";
	const functionMovie2 = "Crouching Tiger, Hidden Dragon";
	const functionMovie3 = "Doctor Strange";
	const functionMovie4 = "Anna and the King";
	const functionMovie5 = "Avengers:End Game";

	let functionRatingMovie1 = "85%";
	let functionRatingMovie2 = "97%";	
	let functionRatingMovie3 = "74%";
	let functionRatingMovie4 = "51%";
	let functionRatingMovie5 = "94%";

	
	console.log (1 + ". " + functionMovie1);
	console.log ("Rotten Tomatoes Rating: " + functionRatingMovie1);
	console.log(2 + ". " + functionMovie2);
	console.log ("Rotten Tomatoes Rating: " + functionRatingMovie2);
	console.log(3 + ". " + functionMovie3);
	console.log ("Rotten Tomatoes Rating: " + functionRatingMovie3);
	console.log(4 + ". " + functionMovie4);
	console.log ("Rotten Tomatoes Rating: " + functionRatingMovie4);
	console.log(5 + ". " + functionMovie5);
	console.log ("Rotten Tomatoes Rating: " + functionRatingMovie5);

}

favoriteMovies();


/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/



let printFriends = function(){

	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:");
	console.log(friend1)
	console.log(friend2)
	console.log(friend3)
	
}
printFriends();


